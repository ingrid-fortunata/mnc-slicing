# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
The styling is using [Tailwind](https://tailwindcss.com/) in order to make the development faster.
The library that used for image slider (in order to make it faster to develop) is [Swiper](https://www.npmjs.com/package/swiper) because is light weighted.

## How to run this project

You can run this project by running the command : `npm start` to run the app in the development mode. To view it on your browser, open [http://localhost:3000](http://localhost:3000).

As per react documentation too, you can run command : `npm build` to run build the app for production to the `build` folder.

# Folder structure

On the `src` folder you could see the folder structure as per below :

- `style` : to input the `global.css` file as global styling for all the files.
- `layout` : based on the mock figma that there's a column part (right & left part) -- this one is for layouting the column part.
- `components` : to store all the components. Per components folder will has :
  - `assets` folder : to store all the image assets -- better to put it like this because 1 component has different assets from the others.

* Kindly note that there's a `shared` folder inside the `components` folder for **reusable** components.

# What have done

- Layouting the layout
- Make some reusable components for the topic title & news box/article
- Responsive to the mobile design

## What need to be improved

- Column layout and reusable components already done, but not yet all finished per topic section layouting as per figma design. But overall can be seen the column layout. If there's more time (2 days more) I think it can be finished all with the responsiveness.
- Functionality (if any)
