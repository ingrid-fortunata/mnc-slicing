import Footer from "./components/footer";
import Header from "./components/header";
import MainNews from "./components/main-news";
import ScheduleResult from "./components/scheduleResult";
import ColumnLayout from "./layout/column-layout";

function App() {
  return (
    <div className="App">
      <Header />
      <ScheduleResult />
      <MainNews />
      <ColumnLayout />
      <Footer />
    </div>
  );
}

export default App;
