import React from "react";
import HotTopics from "../components/hot-topics";
import LatestNews from "../components/latest-news";
import OtherNews from "../components/other-news";

function ColumnLayout() {
  return (
    <div className="mt-[30px] sm:mt-[35px] sm:mb-[25px] sm:mx-[153px] sm:flex">
      <div className="sm:w-3/4 sm:mr-[30px]">
        <OtherNews />
        <LatestNews />
      </div>
      <div className="sm:w-1/4 px-5 my-[49px] sm:px-0 sm:my-0">
        <HotTopics />
      </div>
    </div>
  );
}

export default ColumnLayout;
