import React from "react";
import Title from "../shared/title";

function HotTopics() {
  const data = [
    "Sepak Bola",
    "Moto GP",
    "Bola Basket",
    "Badminton",
    "Tenis",
    "Formula 1",
  ];
  return (
    <div>
      <Title title="hot topics" />

      <div className="mt-2.5">
        {data.map((el, index) => {
          const isOdds = index === 0 || (index - 1) % 2 !== 0;
          return (
            <div
              key={index}
              className={`flex text-white p-3.5 items-center ${
                isOdds ? "bg-[#070D59]" : "bg-[#24326F]"
              }`}
            >
              <svg
                width="7"
                height="11"
                viewBox="0 0 7 11"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1.00003 1.17261L6.00001 5.12799"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.00003 1.17261L6.00001 5.12799"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.00003 1.17261L6.00001 5.12799"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.00003 1.17261L6.00001 5.12799"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.11703 8.98462L5.99998 5.12803"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.11703 8.98462L5.99998 5.12803"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.11703 8.98462L5.99998 5.12803"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
                <path
                  d="M1.11703 8.98462L5.99998 5.12803"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                />
              </svg>
              <div className="ml-3.5">{el}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default HotTopics;
