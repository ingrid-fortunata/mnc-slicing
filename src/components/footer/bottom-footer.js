import React from "react";

function BottomFooter() {
  return (
    <div className="bg-[#070D59] text-center sm:py-5 py-2.5 text-[#CCCCCC] text-xl break-words">
      &#169; Copyrights 2021. Sportstars.id All rights reserved
    </div>
  );
}

export default BottomFooter;
