import React from "react";
import NewsBox from "../shared/news-box";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";

function MainNews() {
  const data = [
    {
      image: require("./assets/news1.png"),
      topic: "bola",
      date: "Rabu, 30 Maret 2020",
      title:
        "Gara-Gara Santiago Bernabeu, EL Real Batal Datangkan Cristiano Ronaldo?",
      description:
        "SPANYOL - Presiden Real Madrid, Ramon Calderon, agak ragu mantan timnya belanja jorjoran pada bursa transfer musim panas 2021",
    },
    {
      image: require("./assets/news2.png"),
      topic: "bola",
      date: "Rabu, 30 Maret 2020",
      club: "Persib Vs Persiraja",
      title: "Pangeran Biru Ingin Terus Ikuti Piala Menpora",
    },
    {
      image: require("./assets/news3.png"),
      topic: "sportainment",
      date: "Rabu, 30 Maret 2020",
      title:
        "Bambang Pamungkas, Legenda Sepanjang Masa Persija dan Timnas Indonesia",
    },
    {
      image: require("./assets/news4.png"),
      topic: "photo",
      date: "Rabu, 30 Maret 2020",
      title: "Ketenaran Pevoli Cantik Sabina Altynbekova Sempat Bikin Khawatir",
    },
    {
      image: require("./assets/news5.png"),
      topic: "balap",
      date: "Rabu, 30 Maret 2020",
      title: "Jelang Seri Kedua MotoGP 2021: Rossi dan Morbidelli Sibuk",
    },
    {
      image: require("./assets/news6.png"),
      topic: "video",
      date: "Rabu, 30 Maret 2020",
      title: "Phoenix Suns Pecundangi Hornets",
    },
  ];

  return (
    <>
      <div className="hidden sm:grid grid-cols-3 gap-2 bg-[#070D59] py-[35px] px-[150px]">
        {data.map((obj, index) => {
          let isFirst = index === 0;
          return (
            <div className={isFirst ? "col-span-2 row-span-2" : ""} key={index}>
              <NewsBox
                image={obj.image}
                topic={obj.topic}
                date={obj.date}
                title={obj.title}
                titleSize={isFirst ? "text-3xl" : "text-xl"}
                description={obj.description}
                club={obj.club}
              />
            </div>
          );
        })}
      </div>

      <div className="sm:hidden bg-[#070D59] py-5 px-2.5">
        <Swiper pagination={true} modules={[Pagination]} className="mySwiper">
          {data.map((obj, index) => {
            return (
              <SwiperSlide key={index}>
                <NewsBox
                  image={obj.image}
                  topic={obj.topic}
                  date={obj.date}
                  title={obj.title}
                  description={obj.description}
                  club={obj.club}
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </>
  );
}

export default MainNews;
