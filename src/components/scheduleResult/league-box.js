import React from "react";

function LeagueBox({
  isLast,
  league,
  date,
  homeIcon,
  home,
  homeScore,
  awayIcon,
  away,
  awayScore,
  status,
}) {
  return (
    <div
      className={
        isLast
          ? "text-black min-w-[151px] space-y-1 border-r border-[#C4C4C4] mb-[21px]"
          : "text-black min-w-[151px] space-y-1 border-r border-[#C4C4C4] mr-6 mb-[21px]"
      }
    >
      <div className="font-medium text-xs">{league}</div>
      <div className="text-[#ED1C24] font-medium text-xs">{date}</div>
      <div className="font-medium text-base text-black grid grid-cols-4">
        <img src={homeIcon} alt="home-icon" width="25px" height="25px" />
        <div className="col-span-2">{home}</div>
        <div>{homeScore}</div>
      </div>
      <div className="font-medium text-base text-black grid grid-cols-4">
        <img src={awayIcon} alt="away-icon" width="25px" height="25px" />
        <div className="col-span-2">{away}</div>
        <div>{awayScore}</div>
      </div>
      <div className="text-sm font-bold text-[#081158]">{status}</div>
    </div>
  );
}

export default LeagueBox;
