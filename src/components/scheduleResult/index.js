import React from "react";
import LeagueBox from "./league-box";

function ScheduleResult() {
  const Data = [
    {
      league: "Liga Inggris Premier League",
      date: "Rabu, 31 Maret 2021",
      home: "BEL",
      homeIcon: require("./assets/belgia.png"),
      homeScore: 0,
      away: "FRA",
      awayIcon: require("./assets/france.png"),
      awayScore: 0,
      status: "Pre Match",
    },
    {
      league: "Liga Inggris Premier League",
      date: "Rabu, 31 Maret 2021",
      home: "NED",
      homeIcon: require("./assets/netherland.png"),
      homeScore: 0,
      away: "ITA",
      awayIcon: require("./assets/italy.png"),
      awayScore: 0,
      status: "Pre Match",
    },
    {
      league: "Liga Inggris Premier League",
      date: "Rabu, 31 Maret 2021",
      home: "GER",
      homeIcon: require("./assets/german.png"),
      homeScore: 0,
      away: "POR",
      awayIcon: require("./assets/portugal.png"),
      awayScore: 0,
      status: "Pre Match",
    },
    {
      league: "Liga Inggris Premier League",
      date: "Rabu, 31 Maret 2021",
      home: "ENG",
      homeIcon: require("./assets/england.png"),
      homeScore: 0,
      away: "SPA",
      awayIcon: require("./assets/spain.png"),
      awayScore: 0,
      status: "Pre Match",
    },
    {
      league: "Liga Inggris Premier League",
      date: "Rabu, 31 Maret 2021",
      home: "NOR",
      homeIcon: require("./assets/norway.png"),
      homeScore: 0,
      away: "GRE",
      awayIcon: require("./assets/greece.png"),
      awayScore: 0,
      status: "Pre Match",
    },
  ];

  return (
    <div className="flex sm:items-center sm:justify-between sm:py-[19px] sm:px-[151px] px-5 flex-col sm:flex-row overflow-x-auto">
      <div className="flex sm:flex-col flex-row mt-[17px] sm:mt-0">
        <div className="font-bold text-[#272121] text-xl leading-[23.44px] text-left mr-[15px] sm:mr-0">
          JADWAL & HASIL
        </div>
        <select className="bg-transparent text-[#272121] outline-none text-base sm:text-xs leading-[14px] font-normal">
          <option value="volvo">Jadwal Terbaru</option>
        </select>
      </div>

      <div className="flex items-center justify-between overflow-x-auto mt-[13px]">
        {Data.map((obj, index) => {
          const isLast = Data.length === index + 1;
          return (
            <LeagueBox
              key={index}
              isLast={isLast}
              league={obj.league}
              date={obj.date}
              homeIcon={obj.homeIcon}
              home={obj.home}
              homeScore={obj.homeScore}
              awayIcon={obj.awayIcon}
              away={obj.away}
              awayScore={obj.awayScore}
              status={obj.status}
            />
          );
        })}

        <div className="border-r border-[#C4C4C4] px-[27px] sm:block hidden">
          <svg
            width="15"
            height="25"
            viewBox="0 0 15 25"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.10001 0.485107L0.973572 1.61155L11.8621 12.5001L0.973572 23.3887L2.10001 24.5151L14.115 12.5001L2.10001 0.485107Z"
              fill="#C8161D"
            />
          </svg>
          <svg
            width="15"
            height="25"
            viewBox="0 0 15 25"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M12.9 24.5149L14.0264 23.3885L3.13787 12.4999L14.0264 1.61133L12.9 0.484892L0.88499 12.4999L12.9 24.5149Z"
              fill="black"
            />
          </svg>
        </div>
      </div>
    </div>
  );
}

export default ScheduleResult;
