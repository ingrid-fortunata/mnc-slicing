import React from "react";

function Title({ title }) {
  return (
    <div className="border-b-4 border-[#ED1C24] pb-2.5 uppercase font-bold text-xl">
      {title}
    </div>
  );
}

export default Title;
