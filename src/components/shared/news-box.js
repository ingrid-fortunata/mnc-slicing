import React from "react";

function NewsBox({
  image,
  topic,
  date,
  title,
  description,
  club,
  titleSize,
  isOtherNews,
  index,
}) {
  return (
    <div className="relative inline-block" key={index}>
      <img src={image} alt="news" className="w-screen sm:w-auto" />
      <div
        className={
          isOtherNews
            ? "gradient-background absolute bottom-0 text-white w-fit px-2 pb-1.5"
            : "gradient-background absolute bottom-0 text-white w-fit px-[30px] pb-1.5"
        }
      >
        <div className="flex items-center">
          <div
            className={
              topic
                ? "px-2.5 py-[5px] bg-[#ED1C24] uppercase mr-5 text-[15px] leading-[18px]"
                : "hidden"
            }
          >
            {topic}
          </div>
          <div className="text-xs">{date}</div>
        </div>
        <div className="text-[#ED2224] text-sm font-medium mt-2.5">{club}</div>
        <div className={`my-[12px] ${titleSize} font-bold`}>{title}</div>
        <div className="font-medium text-base">{description}</div>
      </div>
    </div>
  );
}

export default NewsBox;
