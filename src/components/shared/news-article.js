import React from "react";

function NewsArticle({
  chooseStyle,
  image,
  imageStyle,
  topic,
  date,
  subInfo,
  title,
  description,
}) {
  return (
    <div className={chooseStyle}>
      <img src={image} alt="news" className={imageStyle} />
      <div className="sm:ml-[15px]">
        <div className="text-base font-medium text-[#ED1C24]">{topic}</div>
        <div className="sm:my-3 my-[8px] flex items-center">
          <span>
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M3.5 6C3.5 5.17157 4.17157 4.5 5 4.5H15C15.8284 4.5 16.5 5.17157 16.5 6V7.5H3.5V6Z"
                fill="#443737"
                stroke="#443737"
              />
              <rect
                x="3.5"
                y="5.5"
                width="13"
                height="10"
                rx="1.5"
                fill="white"
                stroke="#443737"
              />
              <circle
                cx="6.25"
                cy="4.25"
                r="1"
                fill="white"
                stroke="#443737"
                strokeWidth="0.5"
              />
              <circle
                cx="13.75"
                cy="4.25"
                r="1"
                fill="white"
                stroke="#443737"
                strokeWidth="0.5"
              />
              <path
                d="M8.43302 10.25C8.29495 10.0109 7.98916 9.92892 7.75001 10.067C7.51087 10.2051 7.42893 10.5109 7.567 10.75L8.43302 10.25ZM13.2349 9.29637C13.4277 9.09871 13.4238 8.78215 13.2262 8.58932C13.0285 8.39648 12.7119 8.4004 12.5191 8.59806L13.2349 9.29637ZM9.25001 12.6651L8.817 12.9151C8.89527 13.0506 9.0325 13.1417 9.18782 13.1612C9.34313 13.1807 9.4986 13.1263 9.60791 13.0142L9.25001 12.6651ZM7.567 10.75L8.817 12.9151L9.68302 12.4151L8.43302 10.25L7.567 10.75ZM9.60791 13.0142L13.2349 9.29637L12.5191 8.59806L8.89211 12.3159L9.60791 13.0142Z"
                fill="#443737"
              />
            </svg>
          </span>
          <span className="text-[#443737] text-xs">{date}</span>
        </div>
        <div className="text-[#ED2224] font-bold text-sm">{subInfo}</div>
        <div className="text-[#272121] text-xl font-bold my-2.5">{title}</div>
        <div className="text-#272121 text-[15px] sm:block hidden">
          {description}
        </div>
      </div>
    </div>
  );
}

export default NewsArticle;
