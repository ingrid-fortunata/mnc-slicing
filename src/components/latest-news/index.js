import React from "react";
import NewsArticle from "../shared/news-article";
import NewsBox from "../shared/news-box";
import Title from "../shared/title";
import News2Img from "./assets/news2.png";

function LatestNews() {
  const data = {
    image: require("./assets/news1.png"),
    topic: "F1",
    date: "31 Maret 2021",
    subInfo: "Kualifikasi Piala Dunia 2022",
    title: "Hamilton Menangi Persaingan Sengit atas Verstappen",
    description:
      "Pembalap Tim Mercedes, Lewis Hamilton, tampil sebagai pemenang di ajang Formula One (F1) GP Bahrain 2021. Akan tetapi, kemenangan ini tak didapat Hamilton dengan mudah karena ia harus bersaing sengit dengan pembalap Red Bull, Max Verstappen.",
  };
  return (
    <div className="mt-9 mx-5 sm:mx-0">
      <Title title="latest news" />

      <NewsArticle
        chooseStyle="sm:flex mt-[30px] sm:items-center"
        image={data.image}
        imageStyle="sm:w-[232px] w-full sm:h-199px h-[227px]"
        topic={data.topic}
        date={data.date}
        subInfo={data.subInfo}
        title={data.title}
        description={data.description}
      />

      <div className="flex items-center sm:mt-[61px] mt-8">
        <NewsBox
          image={News2Img}
          topic="moto gp"
          date="Rabu, 30 Maret 2020"
          title="Disalip Jelang Finis, Joan Mir Hanya Bisa Tersenyum Getir"
          titleSize="sm:text-3xl text-base"
          club="GP Sepang 2021"
        />
      </div>
    </div>
  );
}

export default LatestNews;
