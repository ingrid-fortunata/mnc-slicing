import React, { useState } from "react";
import NewsBox from "../shared/news-box";
import Ads from "./assets/ads.png";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper";
import "swiper/css/pagination";
import "swiper/css";
import "swiper/css/navigation";

function OtherNews() {
  const data = [
    {
      image: require("./assets/news1.png"),
      title: "Anthony Ginting Ungkap Sisi Positif Indonesia Badminton ",
    },
    {
      image: require("./assets/news2.png"),
      title:
        "Jonatan Christie Soroti Venue Practice Hall Jelang Indonesia Badminton Festival",
    },
    {
      image: require("./assets/news3.png"),
      title: "Jelang Indonesia Masters 2021, The Daddies Akui",
    },
    {
      image: require("./assets/news4.png"),
      title: "Jonatan Christie Bersyukur Cederanya Berangsur Pulih",
    },
  ];

  return (
    <>
      <img src={Ads} alt="ads" />
      <div className="hidden sm:flex justify-between mt-7">
        {data.map((obj, index) => {
          return (
            <NewsBox
              index={index}
              image={obj.image}
              title={obj.title}
              titleSize="text-sm"
              isOtherNews
            />
          );
        })}
      </div>

      <div className="sm:hidden mt-5">
        <Swiper
          slidesPerView={3}
          centeredSlides={true}
          spaceBetween={5}
          pagination={{
            type: "fraction",
          }}
          navigation={true}
          modules={[Pagination, Navigation]}
          className="mySwiper"
        >
          {data.map((obj, index) => {
            return (
              <SwiperSlide key={index}>
                <NewsBox
                  image={obj.image}
                  title={obj.title}
                  titleSize="text-xs"
                  isOtherNews
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </>
  );
}

export default OtherNews;
