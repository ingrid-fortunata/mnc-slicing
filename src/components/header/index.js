import React from "react";
import MainHeader from "./main-header/main-header";
import SubHeader from "./sub-header/sub-header";
import TopHeader from "./top-header/top-header";

function Header() {
  return (
    <>
      <div className="sm:block hidden">
        <TopHeader />
      </div>

      <MainHeader />
      <SubHeader />
    </>
  );
}

export default Header;
