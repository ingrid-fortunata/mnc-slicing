import React from "react";
import HeaderLogo from "./assets/HeaderLogo.png";
import HeaderAds from "./assets/HeaderAds.png";

function MainHeader() {
  return (
    <>
      {/* <div className="sm:block hidden"> */}
      <div className="sm:flex items-center justify-around py-5 hidden">
        <img src={HeaderLogo} alt="logo" width="330.89px" height="50px" />

        <img src={HeaderAds} alt="ads" width="728px" height="90px" />
      </div>
      {/* </div> */}

      {/* <div className="sm:hidden block"> */}
      <div className="flex items-center justify-around py-5 sm:hidden">
        <svg
          width="20"
          height="14"
          viewBox="0 0 20 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect y="-0.000244141" width="20" height="2" rx="1" fill="#131313" />
          <rect y="5.99976" width="20" height="2" rx="1" fill="#131313" />
          <rect y="11.9998" width="20" height="2" rx="1" fill="#131313" />
        </svg>

        <img src={HeaderLogo} alt="logo" width="215px" height="32.49px" />

        <svg
          width="20"
          height="20"
          viewBox="0 0 20 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M15.0669 8.46418C15.0667 12.6364 11.8691 15.9284 8.03334 15.9284C4.19754 15.9284 1.00013 12.6364 1.00025 8.46418C1.00037 4.29195 4.19796 1 8.03376 1C11.8696 1 15.067 4.29195 15.0669 8.46418Z"
            stroke="#131313"
            strokeWidth="2"
          />
          <rect
            x="0.692268"
            y="-0.0451739"
            width="0.999238"
            height="8.004"
            rx="0.499619"
            transform="matrix(0.645955 -0.763397 0.739636 0.672981 12.3284 14.4546)"
            fill="#131313"
            stroke="#131313"
            strokeWidth="0.999238"
          />
        </svg>
      </div>
      {/* </div> */}
    </>
  );
}

export default MainHeader;
